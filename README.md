This project was created using react.
This app is diployed with SURGE and you can check it online as well: http://attractive-work.surge.sh/


There are three main pages.

ARTICLES:

    On the first/home one you can see all published (active) versions of articles.
    You can also see pagination, every page display only 6 articles, if you dont have enough please
    create some to enable pagination.

CREATE ARTICLE page:

    here you can insert title and text of the article and save it.
    Saved article will be published and visible on 'Articles page'. By creating new article
    you automatically publish it.

SINGLE ARTICLE page:

    clicking on the article from 'articles' page you can see on the right side all possible
    versions of that article and you can choose any of these.

    You have the possibility to change each article version and by clicking "Save" app will
    save your new version of article with new title and text but within the same article name.

    You can also press 'publish' and this will activate that version and you will now be able to see that version on
    'Articles' page instead of previous one.

    NOTE: Every time you publish one version of article, other versions will be deactivated.

start the app with 'NPM INSTALL NPM START'.

Admin: Jovana Jevtic