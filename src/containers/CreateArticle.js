import React, {Component} from 'react';
import request from "../../node_modules/superagent/superagent";
import './CreateArticle.css';
import Header from "../Header";
import {Link} from "react-router-dom";
import Alert from "../components/Alert";
import Footer from "../components/Footer";

export default class CreateArticle extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      text: '',
      alertShow: false,
      alertMessage: ''
    };
  }

  onChange = (e) => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  onSubmit = (e) => {
    e.preventDefault();
    let self = this;
    const {title, text} = this.state;
    request
      .post('http://46.101.150.155:3512/v1/articles')
      .set('Content-Type', 'application/json')
      .send({title: title, text: text})
      .end(function (err, res) {
        self.setState({
          alertMessage: 'You have successfully saved new article ',
          alertShow: true
        });
        setTimeout(function () {
          self.setState({
            alertMessage: '',
            alertShow: false
          });
        }, 3000);
        console.log(res.text);
      });
    this.setState({
      title: '',
      text: '',
    });
  }


  render() {
    const {title, text} = this.state;
    return (
      <div className="">
        <Header/>
        <div className="container">
          <Alert show={this.state.alertShow} message={this.state.alertMessage}/>
          <div className="">
            <h1>Create new Article</h1>
            <form onSubmit={this.onSubmit} className="form-group">
              <input type="text" name="title" value={title} onChange={this.onChange} className="form-control"
                     placeholder="Title" required/>
              <textarea name="text" value={text} onChange={this.onChange} className="form-control" placeholder="Text"
                        required/>
              <div className="bottom">
                <button type="submit" className="btn btn-primary submit">Submit</button>
                <Link to="/" className="btn btn-primary cancel">Cancel</Link>
              </div>
            </form>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}