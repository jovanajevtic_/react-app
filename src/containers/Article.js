import React from 'react';
import ArticleList from '../components/ArticleList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as articleActions from '../actions/articles';

const Article = ({articles, actions}) => (
  <ArticleList
    articles={articles}
    actions={actions}
  />
);

const mapStateToProps = ({ articles }) => ({
  articles,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(articleActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Article);
