import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
        <nav className="navbar navbar-inverse">
          <div className="container-fluid">
            <ul className="nav navbar-nav">
              <li><Link to="/">Articles</Link></li>
              <li><Link to={`/create-article`}>Create article</Link></li>
            </ul>
          </div>
        </nav>
    );
  }
}