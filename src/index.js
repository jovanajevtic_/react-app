import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Route, BrowserRouter, Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import store from './store/configureStore';
import Article from './containers/Article';
import SingleArticle from './components/SingleArticle';
import CreateArticle from './containers/CreateArticle';

render((
  <BrowserRouter>
    <Provider store={store}>
      <Switch>
        <Route exact path='/' component={Article}/>
        <Route exact path='/create-article' component={CreateArticle}/>
        <Route path="/article/:articleName" component={SingleArticle}/>
      </Switch>
    </Provider>
  </BrowserRouter>
), document.getElementById('root'));
