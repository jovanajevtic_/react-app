import { createStore } from 'redux';
import { persistStore } from 'redux-persist'
import reducers from '../reducers';


function configureStore() {
  const store = createStore(reducers);

  persistStore(store);

  return store;
}

export default configureStore();
