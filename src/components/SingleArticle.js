import React, {Component} from 'react';
import './Article.css';
import './SingleArticle.css';
import Header from "../Header";
import Moment from 'moment';
import request from "../../node_modules/superagent/superagent";
import Footer from "./Footer";

class SingleArticle extends Component {
  constructor() {
    super();
    this.state = {
      articles: [],
      activeArticle: null,
      activeVersion: 0,
      title: '',
      text: '',
    };
    this.changeVersion = this.changeVersion.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.changeText = this.changeText.bind(this);
    this.createArticleWithNewVersion = this.createArticleWithNewVersion.bind(this);
  }

  componentDidMount() {
    const data = {articleName: this.props.match.params.articleName};

    fetch(`http://46.101.150.155:3512/v1/articles/${data.articleName}`)
      .then(response => response.json())
      .then(jsondata => {
        this.setState({
          articles: jsondata,
          activeArticle: jsondata[0],
          activeVersion: jsondata[0].version,
          title: jsondata[0].title,
          text: jsondata[0].text
        })
      })

  }

  changeVersion(event) {
    const article = this.state.articles.find(a => a.version === Number(event.target.value));
    this.setState({
      activeVersion: article.version,
      activeArticle: article,
      title: article.title,
      text: article.text,
    });
  }

  createArticleWithNewVersion(e) {
    e.preventDefault();
    const data = {articleName: this.props.match.params.articleName, version: this.state.activeArticle.version};
    console.log('Publishing article...', data.articleName);
    request
      .post(`http://46.101.150.155:3512/v1/articles/${data.articleName}`)
      .set('Content-Type', 'application/json')
      .send({
        title: this.state.title,
        text: this.state.text
      })
      .end(function (err, res) {
        console.log(res.text);
        window.location.reload();
      });
  };


  publishArticle = (e) => {
    e.preventDefault();
    fetch(`http://46.101.150.155:3512/v1/articles/${this.state.activeArticle.articleName}/${this.state.activeArticle.version}/publish`, {
      method: 'PUT',
      mode: 'CORS',
      body: '',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => {
      window.location.reload();
      return res;
    }).catch(err => err);
  };

  changeText(event) {
    this.setState({
      text: event.target.value
    });
  }


  changeTitle(event) {
    this.setState({
      title: event.target.value
    });
  }

  render() {
    return (
      <div>
        <Header/>
        <div className="container">
          <div className="row row-eq-height">
            {this.state.activeArticle !== null && this.state.activeArticle !== undefined ? (
                <div className="single-article">
                  <div className="col-md-8">
                    <span> {Moment(this.state.activeArticle.updatedAt).format('d. MMM, YYYY')}</span>
                    <input type="text" value={this.state.title} className="form-control"
                           onChange={this.changeTitle}/>
                    <textarea value={this.state.text} className="form-control" onChange={this.changeText}/>
                  </div>
                  <div className="col-md-4">
                    <h5>Choose article version</h5>
                    <select onChange={this.changeVersion} value={this.state.activeArticle.version}
                            className="form-control">
                      {this.state.articles.map(article =>
                        <option key={article.version} value={article.version}>{article.version}</option>
                      )}
                    </select>
                    {this.state.activeArticle.active ? (
                      <h5>This version of article is currently ACTIVE</h5>
                    ) : (
                      <h5>This version of article is NOT ACTIVE</h5>
                    )}
                    <button className="btn btn-primary update" onClick={this.createArticleWithNewVersion}>Save</button>
                    <button className="btn btn-primary publish" onClick={this.publishArticle}>Publish</button>
                  </div>
                </div>)
              : false}
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default SingleArticle;