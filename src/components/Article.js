import React, {Component} from 'react';
import './Article.css';
import {Link} from "react-router-dom";
import Moment from 'moment';

class Article extends Component {
  render() {
    const {text, title, articleName, updatedAt} = this.props;

    return (
      <article className="col-xs-4">
        <div className="article-wrapper">
          <span> {Moment(updatedAt).format('d. MMM, YYYY')}</span>
          <h3><Link to={`/article/${articleName}`}>{title}</Link></h3>
          <p>{text === null ? '' : text.substring(0, 400)}<Link to={`/article/${articleName}`}>  see more...</Link></p>
        </div>
      </article>
    );
  }
}

export default Article;