import React, {Component} from 'react';

export default class Alert extends Component {
  render() {
    const {show, message} = this.props;
    return (
      <div>
        {show ? (
          <div className="alert alert-success alert-dismissible show" role="alert">
            <strong>Well done! </strong>{message}
          </div>
        ) : ('')}
      </div>
    )
  }
}