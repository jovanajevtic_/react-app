import React, {Component} from 'react';
import './Footer.css';

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <span>Made by Jovana Jevtic</span>
      </div>
    )
  }
}