import React, {Component} from 'react';
import Article from './Article';
import Header from "../Header";
import Footer from "./Footer";

export default class ArticleList extends Component {

  constructor() {
    super();
    this.state = {
      articles: [],
      currentPage: 1,
      articlesPerPage: 6
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  componentDidMount() {
    fetch('http://46.101.150.155:3512/v1/articles')
      .then(response => response.json())
      .then(jsondata => {
        console.log(jsondata);
        this.setState({
          articles: jsondata
        })
      });

  }

  render() {
    const {articles, currentPage, articlesPerPage} = this.state;

    const indexOfLastTodo = currentPage * articlesPerPage;
    const indexOfFirstTodo = indexOfLastTodo - articlesPerPage;
    const currentTodos = articles.slice(indexOfFirstTodo, indexOfLastTodo);

    const renderTodos = currentTodos.map(article => {
      return <Article
        key={article.ID}
        id={article.ID}
        articleName={article.articleName}
        title={article.title}
        text={article.text}
        active={article.active}
        updatedAt={article.updatedAt}
      />;
    });

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(articles.length / articlesPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li
          key={number}
          id={number}
          className={this.state.currentPage === number ? 'active' : 'inactive'}
          onClick={this.handleClick}
        >
          {number}
        </li>
      );
    });

    return (
      <div>
        <Header/>
        <div className="container">
          <div className="row row-eq-height">
            {renderTodos}
          </div>
          <ul className="page-numbers">
            {renderPageNumbers}
          </ul>
        </div>
        <Footer/>
      </div>
    );
  }
}