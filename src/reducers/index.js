import { combineReducers } from 'redux';

import articles from './articles';

const reducer = combineReducers({
  articles
});

export default reducer;
