import types from '../actions/actionTypes';

const switchServiceData = fetch('http://46.101.150.155:3512/v1/articles')
  .then(res => {
    res.json();
  });

const initialState = [
  {
    ID: switchServiceData ? switchServiceData.ID : null,
    articleName: switchServiceData ? switchServiceData.articleName : '',
    text: switchServiceData ? switchServiceData.text : '',
  },
];

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case types.GET_ALL:
      return [
        fetch('http://46.101.150.155:3512/v1/articles')
          .then(res => {
            res.json();
          })
      ];

    default:
      return state;
  }
}

