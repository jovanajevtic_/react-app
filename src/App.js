import React, { Component } from 'react';
import './App.css';
import Header from './Header'

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
      </div>
    );
  }
}
